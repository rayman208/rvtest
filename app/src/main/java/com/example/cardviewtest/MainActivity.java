package com.example.cardviewtest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    private ArrayList<Person> persons;
    private RecyclerView rv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        persons = new ArrayList<>();
        persons.add(new Person("Emma Wilson", "23 years old", R.drawable.emma));
        persons.add(new Person("Lavery Maiss", "25 years old", R.drawable.lavery));
        persons.add(new Person("Lillie Watts", "35 years old", R.drawable.lillie));

        rv = findViewById(R.id.rv);
        //rv.addItemDecoration(null);
        //rv.addItemDecoration(new DividerItemDecoration(this, 0));

        GridLayoutManager llm = new GridLayoutManager(this,2);
        rv.setLayoutManager(llm);
        //rv.setHasFixedSize(true);

        RvAdapter adapter = new RvAdapter(persons);
        rv.setAdapter(adapter);
    }
}